require 'nokogiri'
require 'csv'

CURRENT_ELECTION_PERIOD = '19'.freeze

rows = [%w[salutation first_name last_name email party]]

# https://www.bundestag.de/services/opendata
doc = Nokogiri::XML(File.open('data/raw/MDB_STAMMDATEN.XML'))
doc.xpath("//MDB").each do |mdb|
  next unless mdb.css('WP').select {|wp| wp.text == CURRENT_ELECTION_PERIOD}.any?

  gender = mdb.css('GESCHLECHT').text
  salutation = gender == 'männlich' ? 'Lieber Herr' : 'Sehr verehrte, liebe Frau'
  first_name = mdb.css('VORNAME').last.text.split(' ').first
  last_name = mdb.css('NACHNAME').last.text
  email = "#{first_name}.#{last_name}@bundestag.de".downcase.gsub(/[äöü]/) do |match|
    case match
    when "ä"
      'ae'
    when "ö"
      'oe'
    when "ü"
      'ue'
    end
  end
  party = mdb.css('PARTEI_KURZ').text
  rows << %W[#{salutation} #{first_name} #{last_name} #{email} #{party}]
end

return unless rows.any?

File.open("data/people.csv", "w") do |f|
  f.write(rows.inject([]) do |csv, row|
    csv << CSV.generate_line(row)
  end.join(""))
end
