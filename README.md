# Simple Ruby Script To Mail All (current) Members Of The German Bundestag

# Requirements

* Ruby installed. See https://www.ruby-lang.org/en/documentation/installation/ for details.
* Bundler installed. See https://bundler.io/. Basically run `gem install bundler` in this dir.
* Git installed. See https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
* Defaults to Gmail Accounts. Please make sure you allow less secure apps. See https://myaccount.google.com/lesssecureapps

# Get the code
Run `git clone git@gitlab.com:kohpeiss/dbtag_mailer.git` in a terminal to get the sources.

# Configuration
First, adjust the settings and content in `mail_mdbs.rb` by replacing the `YOUR_` placeholders with your data:

| Placeholder  |  Description  | Example |
|--------------|---|---|
| YOUR_EMAIL   | your gmail email address | jane.doe@gmail.com |
| YOUR_NAME    | first name last name | Jane Doe |
| YOUR_PASSWORD | your password for your Google account | TopSecret! |
| YOUR_SUBJECT | the subject of your email | something meaningful |
| YOUR_TEXT    | The text of the email you want to send. Keep the `#{person['salutation']} #{person['first_name']} #{person['last_name']},` line, this data comes from the `data/people.csv` file | long text with cool content |

If you like to attach file(s), add the attachment to `files` dir - e.g. `files/YOUR_ATTACHMENT.pdf` and change `# add_file './files/'` to `add_file './files/YOUR_ATTACHMENT.pdf'`.

Don't forget to save the changes ;-)

If you want to test the sending first, uncomment this
`# csv = [{ 'salutation' => 'Hallo', 'first_name' => 'Hoshy', 'last_name' => 'Honk', 'email' => 'YOUR_EMAIL' }]`

# Usage

* Type `bundle install` in a terminal to install dependencies. This only needs to be ran once.
* Type `bundle exec ruby mail_mdbs.rb` and you should see some output and counting numbers from 0 to 666.
