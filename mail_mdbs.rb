# frozen_string_literal: true

require 'mail'
require 'csv'

options = {
  address: 'smtp.gmail.com',
  port: 587,
  user_name: 'YOUR_EMAIL',
  password: 'YOUR_PASSWORD',
  authentication: 'plain',
  enable_starttls_auto: true
}

Mail.defaults do
  delivery_method :smtp, options
end

csv_text = File.read('data/people.csv')
csv = CSV.parse(csv_text, headers: true)

# for testing, uncomment and adjust this
# csv = [{ 'salutation' => 'Hallo', 'first_name' => 'Hoshy', 'last_name' => 'Honk', 'email' => 'YOUR_EMAIL' }]

csv.each_with_index do |person, index|
  puts index
  
  next unless person['email']
  
  email = person['email'].strip

  text = <<~TEXT
    #{person['salutation']} #{person['first_name']} #{person['last_name']},

    YOUR_TEXT

  TEXT

  Mail.deliver do
    to email
    from 'YOUR_NAME <YOUR_EMAIL>'
    subject 'YOUR_SUBJECT'
    body text
    # add_file './files/'
  end

  sleep 2
end
