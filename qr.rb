# frozen_string_literal: true

require 'rqrcode'

emails = %w[foo@example.com bar@example.com]
subject = "YOUR SUBJECT HERE"
body = "YOUR BODY HERE"

qrcode = RQRCode::QRCode.new("mailto:#{emails.join(',')}?subject=#{subject}&body=#{body}")

# NOTE: showing with default options specified explicitly
png = qrcode.as_png(
  bit_depth: 1,
  border_modules: 4,
  color_mode: ChunkyPNG::COLOR_GRAYSCALE,
  color: 'black',
  file: nil,
  fill: 'white',
  level: :l,
  mode: :number,
  module_px_size: 6,
  resize_exactly_to: false,
  resize_gte_to: false,
  version: 40
)

IO.binwrite('/tmp/qrcode.png', png.to_s)

`open /tmp/qrcode.png`
